import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 *
 * Describes the book cipher algorithm
 *
 * @author Alex Chernenkov
 */
public class BookCipher{

    // Name of file with plain text
    private String plaintextFilename;

    // Name of file for cipher text output
    private String outputFilename;

    // Name of file with page for encryption
    private String pageFilename;

    // Name of file for decrypted text
    private String decryptionFilename;

    // Result of encryption in String array
    private String[] result;

    // Lines of plain text in List<String>
    //private List<String> linesPlainText;

    // Lines of page for encryption in List<String>
    private List<String> linesPage;

    // Flag to deter,ine if files were read
    private boolean read;

    // 2-D array of Pair(stringNumber, letterNumber) to keep cipher text
    private Pair[][] cipher;

    /**
     * Constructor
     *
     * @param plaintext Name of file with plaintext
     * @param output    Name of file for encrypted text
     * @param page      Name of file with page
     * @param decrypt   Name of file for decrypted text
     */
    BookCipher(String plaintext, String output, String page, String decrypt){
        plaintextFilename = plaintext;
        outputFilename = output;
        pageFilename = page;
        decryptionFilename = decrypt;
        read = false;
    }

    /**
     * Writes array of strings to file
     * @param filename      name of file to write in
     * @param arr           array
     * @throws IOException  in case of write error
     */
    public static void writeStringArray (String filename, String[] arr) throws IOException{
        PrintWriter pw = new PrintWriter(filename);
        for(int i = 0; i < arr.length; i++){
            pw.println("" + arr[i]);
        }
        pw.flush();
        pw.close();
    }


    /**
     * Encrypts the plaintext by the page text
     */
    public void encryptFile(String plain, String page, String output){
        int randomString;
        try{
            List<String> linesPlainText = Files.readAllLines(Paths.get(plain), StandardCharsets.UTF_8);
            List<String> linesPage = Files.readAllLines(Paths.get(page), StandardCharsets.UTF_8);
            result = new String[linesPlainText.size()];
            cipher = new Pair[linesPlainText.size()][256];
            for(int i = 0; i < linesPlainText.size(); i++) result[i] = "";
            int pageSize = linesPage.size();
            for(int i = 0; i < linesPlainText.size(); i++){
                String tmpPlain = linesPlainText.get(i); // i'th string of plaintext
                for(int j = 0; j < tmpPlain.length(); j++){
                    randomString = ((new Random().nextInt()) % pageSize + pageSize) % pageSize;
                    while(!(linesPage.get(randomString).contains("" + tmpPlain.charAt(j)))){
                        randomString = ((new Random().nextInt()) % pageSize + pageSize) % pageSize;
                        if((j > 0) && (randomString == cipher[i][j-1].getString())){
                            randomString = ((new Random().nextInt()) % pageSize + pageSize) % pageSize;
                        }
                    }
                    cipher[i][j] = new Pair(randomString, linesPage.get(randomString).indexOf(tmpPlain.charAt(j)));
                    result[i] += cipher[i][j].modToString();
                    if((cipher[i] == null) || (result[i] == null)) System.out.println("null writing!");
                    //System.out.println(linesPage.get(randomString) + ":" + randomString);
                }
            }
            writeStringArray(output, result);
            System.out.println("File successfully written!\n");
            for (int i = 0; i < result.length; i++) System.out.println(result[i]);
        }catch(IOException ioexc){
            System.out.println(ioexc);
        }
    }

    /**
     * Decrypts early encrypted file.
     * Works only if encryption took place
     */
    public void decryptFile(String ciphertext, String page, String output){
        try{
            int i, j, k;
            List<String> linesCipherText = Files.readAllLines(Paths.get(ciphertext), StandardCharsets.UTF_8);
            List<String> linesPage = Files.readAllLines(Paths.get(page), StandardCharsets.UTF_8);
            Pair cipher[][] = new Pair[linesCipherText.size()][256];
            String result[] = new String[256];
            for(i = 0; i < 256; i++) result[i] = "";
            for(k = 0; k < linesCipherText.size(); k++){
               String[] tmpCipherLine = linesCipherText.get(k).split(";");
               for(i = 0; i < tmpCipherLine.length;i++){
                   cipher[k][i] = new Pair(Integer.parseInt(tmpCipherLine[i].split("-")[0]), Integer.parseInt(tmpCipherLine[i].split("-")[1]) - 1);
                   if(cipher[k][i] != null)result[k] += linesPage.get(cipher[k][i].getString()).charAt(cipher[k][i].getLetter());
//                   System.out.println(cipher[k][i].toString());
               }
               System.out.println(result[k]);
            }
            writeStringArray(output, result);
        }catch (IOException exc){
            System.out.println(exc);
        }catch (NullPointerException nexc){
            nexc.printStackTrace();
        }
        //res[i] += linesPage.get(cipher[i][j].getString()).charAt(cipher[i][j].getLetter());

    }
}