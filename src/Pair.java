/**
 * Describes one letter with pair of numbers:
 * number of string and number of letter in that string in page text
 */
public class Pair {

    // Number of string in page text
    private int string;

    // Number of letter in string
    private int letter;

    /**
     * Constructor
     * @param str Number of string in page text
     * @param ltr Number of letter in string
     */
    Pair(int str, int ltr){
        string = str;
        letter = ltr;
    }

    /**
     * Getter for string
     * @return number of string in page text
     */
    public int getString(){
        return string;
    }

    /**
     * Getter for letter
     * @return number of letter in string
     */
    public int getLetter() { return letter; }

    /**
     * Overrided toString() to present Pair() as String
     * @return String, describing Pair
     */
    @Override
    public String toString() {
        return (string + "-" + letter + ";");
    }

    /**
     * Modified to string. The difference is that letters in string are numerated from 0.
     * This one return normal letter's number, as if letters were numbered from 1.
     * @return String, describing Pair
     */
    public String modToString(){
        return (string + "-" + (letter + 1) + ";");
    }
}
