import java.io.IOException;

class TestClass{
    public static void main(String[] args) {
        BookCipher cipher = new BookCipher(
                "/home/alex/IdeaProjects/CRYPTO1/res/plaintext",
                "/home/alex/IdeaProjects/CRYPTO1/res/ciphertext",
                "/home/alex/IdeaProjects/CRYPTO1/res/page",
                "/home/alex/IdeaProjects/CRYPTO1/res/decrypted");

        cipher.decryptFile("/home/alex/IdeaProjects/CRYPTO1/res/ciphertext",
                "/home/alex/IdeaProjects/CRYPTO1/res/page",
                "/home/alex/IdeaProjects/CRYPTO1/res/decrypted");

//        cipher.encryptFile("/home/alex/IdeaProjects/CRYPTO1/res/plaintext",
//                "/home/alex/IdeaProjects/CRYPTO1/res/page",
//                "/home/alex/IdeaProjects/CRYPTO1/res/ciphertext");
    }
}